package App;

import App.model.AssignedClass;
import App.model.Class;
import App.model.Student;
import App.repository.AssignedClassRepository;
import App.repository.ClassRepository;
import App.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by gabriel morales magnus.
 */
@Configuration
@ComponentScan
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public CommandLineRunner demo(StudentRepository studentRepository, ClassRepository classRepository,
                                  AssignedClassRepository assignedClassRepository) {
        return (args) -> {
            studentRepository.save(new Student("student1","lastname"));
            studentRepository.save(new Student("student2","lastname"));
            studentRepository.save(new Student("student3","lastname"));
            studentRepository.save(new Student("student4","lastname"));
            studentRepository.save(new Student("student5","lastname"));
            studentRepository.save(new Student("student6","lastname"));
            studentRepository.save(new Student("student7","lastname"));
            studentRepository.save(new Student("student8","lastname"));

            classRepository.save(new Class("code1","class1","description of the class"));
            classRepository.save(new Class("code2","class2","description of the class"));
            classRepository.save(new Class("code3","class3","description of the class"));
            classRepository.save(new Class("code4","class4","description of the class"));
            classRepository.save(new Class("code5","class5","description of the class"));
            classRepository.save(new Class("code6","class6","description of the class"));
            classRepository.save(new Class("code7","class7","description of the class"));
            classRepository.save(new Class("code8","class8","description of the class"));

            assignedClassRepository.save(new AssignedClass((long) 1,"code1"));
            assignedClassRepository.save(new AssignedClass((long)1,"code2"));
            assignedClassRepository.save(new AssignedClass((long)1,"code3"));
            assignedClassRepository.save(new AssignedClass((long)1,"code4"));
            assignedClassRepository.save(new AssignedClass((long)1,"code5"));
            assignedClassRepository.save(new AssignedClass((long)1,"code6"));
            assignedClassRepository.save(new AssignedClass((long)1,"code7"));
            assignedClassRepository.save(new AssignedClass((long)1,"code8"));
            assignedClassRepository.save(new AssignedClass((long) 2,"code1"));
            assignedClassRepository.save(new AssignedClass((long) 2,"code2"));
            assignedClassRepository.save(new AssignedClass((long) 2,"code3"));
            assignedClassRepository.save(new AssignedClass((long) 2,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 3,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 4,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 5,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 6,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 7,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 8,"code4"));
            assignedClassRepository.save(new AssignedClass((long) 8,"code5"));
        };
    }

}

