package App.repository;

import App.model.Class;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Repository
public interface ClassRepository extends CrudRepository<Class, String> {

    List<Class> findByTitle(String title);
    List<Class> findByDescription(String description);
}
