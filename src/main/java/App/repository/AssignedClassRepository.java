package App.repository;

import App.model.AssignedClass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Repository
public interface AssignedClassRepository extends CrudRepository<AssignedClass, Long> {

    List<AssignedClass> findByStudentId(Long studentClassId);
    List<AssignedClass> findByStudentClassId(String studentClassId);
}
