package App.repository;

import App.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    List<Student> findByLastName(String lastName);
    List<Student> findByFirstName(String firstName);
}
