package App.controller;

import App.Exceptions.ErrorResponse;
import App.Exceptions.GenericException;
import App.model.Class;
import App.repository.ClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Controller
@RequestMapping("/class")
@Component
public class StudentClassController {

    @Autowired
    private ClassRepository repository;


    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody List<Class> getAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Class> getOne(@PathVariable("id") String id) throws GenericException{
        Class s=repository.findOne(id);
        if(s==null){
            throw new GenericException("Error Not Found");
        }else{
            return new ResponseEntity<Class>(s,HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Class> add(@RequestBody Class aClass) throws GenericException{
        Class s=repository.save(aClass);
        if(s==null){
            throw new GenericException("Error Not Saved");
        }else{
            return new ResponseEntity<Class>(s,HttpStatus.OK);
        }

    }

    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Class> update(@RequestBody Class aClass)
            throws GenericException{
        if(repository.exists(aClass.getCode())){
            Class s=repository.save(aClass);
            if(s==null){
                throw new GenericException("Error Not Saved");
            }else{
                return new ResponseEntity<Class>(s,HttpStatus.OK);
            }
        }else{
            throw new GenericException("Error Not Found");
        }

    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable("id") String id) {
        repository.delete(id);

        return "OK";

    }

    @RequestMapping(value = "/byDescription", method = RequestMethod.POST)
    public @ResponseBody List<Class> getByDescription(@RequestBody Class student) {
        return repository.findByDescription(student.getDescription());
    }
    @RequestMapping(value = "/byTitle", method = RequestMethod.POST)
    public @ResponseBody List<Class> getByTitle(@RequestBody Class student) {
        return repository.findByTitle(student.getTitle());
    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

}
