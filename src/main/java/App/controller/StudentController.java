package App.controller;

import App.Exceptions.ErrorResponse;
import App.Exceptions.GenericException;
import App.model.Student;
import App.repository.StudentRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Controller
@RequestMapping("/student")
@Component
public class StudentController {

    @Autowired
    private StudentRepository repository;

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody List<Student> getAll() {
        return  Lists.newArrayList(repository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Student> getOne(@PathVariable("id") Long id)
    throws GenericException{
        Student s=repository.findOne(id);
        if(s==null){
            throw new GenericException("Error Not Found");
        }else{
            return new ResponseEntity<Student>(s,HttpStatus.OK);
        }


    }
    @RequestMapping(value = "/byFirstName", method = RequestMethod.POST)
    public @ResponseBody List<Student> getByName(@RequestBody Student student) {
        return repository.findByFirstName(student.getFirstName());
    }
    @RequestMapping(value = "/byLastName", method = RequestMethod.POST)
    public @ResponseBody List<Student> getByLastName(@RequestBody Student student) {
        return repository.findByLastName(student.getLastName());
    }
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Student> add(@RequestBody Student studentClass) throws GenericException{
        Student s=repository.save(studentClass);
        if(s==null){
            throw new GenericException("Error Not Saved");
        }else{
            return new ResponseEntity<Student>(s,HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Student> update(@RequestBody Student studentClass) throws GenericException{
        if(repository.exists(studentClass.getStudentId())){
            Student s=repository.save(studentClass);
            if(s==null){
                throw new GenericException("Error Not Saved");
            }else{
                return new ResponseEntity<Student>(s,HttpStatus.OK);
            }
        }else{
            throw new GenericException("Error Not Found");
        }
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable("id") Long id) {
        repository.delete(id);

        return "OK";
    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

}
