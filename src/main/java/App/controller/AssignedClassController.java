package App.controller;

import App.Exceptions.ErrorResponse;
import App.Exceptions.GenericException;
import App.model.AssignedClass;
import App.model.Class;
import App.model.Student;
import App.repository.AssignedClassRepository;
import App.repository.ClassRepository;
import App.repository.StudentRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabriel morales magnus.
 */
@Controller
@RequestMapping("/assigned")
@Component
public class AssignedClassController {

    @Autowired
    private ClassRepository repositoryClass;
    @Autowired
    private StudentRepository repositoryStudent;
    @Autowired
    private AssignedClassRepository repository;


    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    List<AssignedClass> getAll() {
        return Lists.newArrayList(repository.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody AssignedClass getOne(@PathVariable("id") Long id) {
        return repository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<AssignedClass> add(@RequestBody AssignedClass object)  throws GenericException{
        if(repositoryStudent.exists(object.getStudentId())){
            if(repositoryClass.exists(object.getStudentClassId())){
                AssignedClass s=repository.save(object);
                if(s==null){
                    throw new GenericException("Error Not Saved");
                }else{
                    return new ResponseEntity<AssignedClass>(s,HttpStatus.OK);
                }
            }else{
                throw new GenericException("Error Class Not Found");
            }
        }else{
            throw new GenericException("Error Student Not Found");
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<AssignedClass> update(@RequestBody AssignedClass object) throws GenericException{
        if(repository.exists(object.getAssignedId())){
            AssignedClass s=repository.save(object);
            if(s==null){
                throw new GenericException("Error Not Saved");
            }else{
                return new ResponseEntity<AssignedClass>(s,HttpStatus.OK);
            }
        }else{
            throw new GenericException("Error Not Found");
        }
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable("id") Long id) {
        repository.delete(id);

        return "OK";

    }

    @RequestMapping(value = "/byStudent", method = RequestMethod.POST)
    public @ResponseBody List<Class> getAllByStudent(@RequestBody Student student) {
        List<Class>res=new ArrayList<>();
        List<AssignedClass>filtered=repository.findByStudentId(student.getStudentId());
        for(AssignedClass object:filtered){
            res.add(repositoryClass.findOne(object.getStudentClassId()));
        }

        return res;
    }
    @RequestMapping(value = "/byClass", method = RequestMethod.POST)
    public @ResponseBody List<Student> getAllByClass(@RequestBody Class aClass) {
        List<Student>res=new ArrayList<>();
        List<AssignedClass>filtered=repository.findByStudentClassId(aClass.getCode());
        for(AssignedClass object:filtered){
            res.add(repositoryStudent.findOne(object.getStudentId()));
        }

        return res;
    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

}
