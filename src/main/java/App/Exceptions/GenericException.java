package App.Exceptions;

/**
 * Created by gabriel morales magnus.
 */
public class GenericException extends Exception {
    private static final long serialVersionUID = 1L;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }
    public GenericException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }
    public GenericException() {
        super();
    }
}

