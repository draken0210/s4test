package App.model;

import javax.persistence.*;

/**
 * Created by gabriel morales magnus.
 */
@Entity
public class AssignedClass {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long assignedId;
    private Long studentId;
    private String studentClassId;

    public AssignedClass() {
    }

    public AssignedClass(Long studentId, String studentClassId) {
        this.studentId = studentId;
        this.studentClassId = studentClassId;
    }

    public Long getAssignedId() {
        return assignedId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public String getStudentClassId() {
        return studentClassId;
    }
}

