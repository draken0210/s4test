package App.model;

/**
 * Created by gabriel morales magnus.
 */

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Class {

    @Id
    private String code;
    private String title;
    private String description;

    public Class(String code, String title, String description) {
        this.code = code;
        this.title = title;
        this.description = description;
    }

    public Class() {
    }

    @Override
    public String toString() {
        return "Class{" +
                "code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
