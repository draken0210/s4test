package App;

/**
 * Created by gabriel morales magnus.
 */
import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import App.model.Student;
import App.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestRepository {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private StudentRepository students;

    @Test
    public void testFindByLastName() {
        Student customer = new Student("first", "last");
        entityManager.persist(customer);

        List<Student> findByLastName = students.findByLastName(customer.getLastName());

        assertThat(findByLastName).extracting(Student::getLastName).containsOnly(customer.getLastName());
    }
    @Test
    public void testFindByFirstName() {
        Student customer = new Student("first", "last");
        entityManager.persist(customer);

        List<Student> fintByFirstName = students.findByFirstName(customer.getFirstName());

        assertThat(fintByFirstName).extracting(Student::getFirstName).containsOnly(customer.getFirstName());
    }
    @Test
    public void testFindByAll() {
        Student customer = new Student("first", "last");
        entityManager.persist(customer);

        Iterable<Student> find = students.findAll();

        assertThat(find).hasSize(1);

        Student customer2 = new Student("first", "last");
        entityManager.persist(customer2);

        Iterable<Student> find2 = students.findAll();

        assertThat(find2).hasSize(2);
    }
}

